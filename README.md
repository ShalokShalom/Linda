# Linda ( The Cosmos Programming Language )

### Linda is a transpiler from itself to Julia, Python, Haxe and other languages.

Its language design is inspired by Julia, as its combining multiple dispatch, a flexible and complete type system, 
and a simple syntax.  
The language is mainly aimed at beginners, pragmatists and people who value readability, documentation and community. 
